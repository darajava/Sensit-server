# Sensit server

```bash
# Run the API server
npm run start

# Run the websocket server
node chat.js

# start the db
mongod
```

See `start.sh` for details. TODO: This script needs to be made a lot more robust, possibly use Google Cloud. 

Currently all of these ports are hardcoded. The db uses the default `27017`, API is on `1337`, chat is on `1338`.