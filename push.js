let FCM = require('fcm-node');
// Todo: this should probably be hidden
let serverKey = 'AAAA1e23hBM:APA91bE2B0dJW0djQ8sb8j-UlAoVN3hey1tq1zXTgzJ_ZuWr7O0pLLoKXrn-oFVlVqLS3bP-Zc2p5eewsa7FZJg4xt93U91weS2t1d4gAzXJGqydXU1tLGXja_x8iAbgOhy4jCwxzLaEQk5qGhQFKCp0Y5I17uovfg'; //put your server key here
let fcm = new FCM(serverKey);

let User = require('./models/user');
let Message = require('./models/message');
var _ = require('underscore');

let Push = require('./push')

const push = (senderId, forIds, text, sensitive) => {
  User.findOne({_id: senderId}, (err, user) => {
    let sender = user.username;
    User.find({_id: forIds}, (err, users) => {
      for (let i = 0; i < users.length; i++) {
        if (users[i]._id === senderId) break;

        // Message.find({
        //   forUsers: users[i]._id,
        //   seenBy: {$not: users[i]._id},
        // }).exec(function (err, results) {
        //   // let count = results.length
        // });
        sendMessage(sender, users[i].fcmToken, _.unescape(text), sensitive);

      }
    });
  });
  
}

const sendMessage = (sender, token, text, sensitive) => {
  if (!token || !token.length) return;

  if (sensitive) text = "🔒 Sensitive Message";

  let message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
      to: token, 
      collapse_key: 'your_collapse_key',
      
      notification: {
          title: sender, 
          body: text, 
          ledColor: [225, 245, 254, 1],
      },
      
      data: {  //you can send only notification or only data(or include both)
          my_key: 'my value',
          my_another_key: 'my another value'
      }
  };

  fcm.send(message, (err, response) => {
      if (err) {
          console.log("Something has gone wrong!");
      } else {
          console.log("Successfully sent with response: ", response);
      }
  });
}

module.exports.push = push;
